var express = require('express');
var path = require('path');
var indexRouter = require('./routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//socket
const server = require('http').createServer(app);                                              // khởi tạo server
const io = require('socket.io')(server);
const users = {};

io.on('connection', socket => {                                                                // bắt sự kiện khi có một user kết nối đến
  socket.on('new-user', name => {                                                              // thực thi khi client yêu cầu server thực hiện sự kiện có tên là new-user
    users[socket.id] = name
    socket.broadcast.emit('user-connected', name)                                              // server yêu cầu client thực hiện sự kiện user-connected
  })
  socket.on('send-chat-message', message => {                                                  // thực thi khi client yêu cầu server thực hiện sự kiện có tên là send-chat-message
    console.log(socket.id);
    socket.broadcast.emit('chat-message', { message: message, name: users[socket.id] })        // server yêu cầu client thực hiện sự kiện chat-message
  })
})

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

server.listen(3000, '192.168.1.170');                                                           // gán địa chỉ cho server
