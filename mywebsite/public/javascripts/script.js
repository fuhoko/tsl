const socket = io('http://192.168.1.170:3000/')                     // tạo connect với server
const messageContainer = $('#message-container')
const messageForm = $('#send-container')
const messageInput = $('#message-input')

const name = prompt('What is your name?')                           // lấy tên người dùng
appendMessage('You joined')                                         // tạo thông báo bạn đã kết nối thành công
socket.emit('new-user', name)                                       // yêu cầu server thực hiện sự kiện new-user

socket.on('user-connected', name => {                               // tạo thông báo khi có người dùng khác kết nối
  appendMessage(`${name} connected`)
})

socket.on('chat-message', data => {                                 // nhận tin nhắn và xuất ra tin nhắn của người khác gửi
  appendMessage(`${data.name}: ${data.message}`)
})

messageForm.submit( (e) => {
  e.preventDefault()
  const message = messageInput.val();
  appendMessage(`You: ${message}`)
  socket.emit('send-chat-message', message)                         // gửi tin nhắn lên server
  messageInput.val('');
})

function appendMessage(message) {                                   // function sử dụng để in ra tin nhắn ra màn hình
  const messageElement = document.createElement('div')
  messageElement.innerText = message
  messageContainer.append(messageElement)
}